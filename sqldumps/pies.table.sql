CREATE TABLE IF NOT EXISTS `pies` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `pie_name` varchar(255),
  `main_ingredient` varchar(255),
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("cheese cake","cheese");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("apple pie","sugar");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("pecan pie","sugar");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("pumpkin pie","fruit or vegetable");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("key lime pie","fruit or vegetable");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("lemon meringue pie","fruit or vegetable");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("sugar cream pie","sugar");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("sweet potato pie","fruit or vegetable");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("strawberry pie","fruit or vegetable");
INSERT INTO `pies` (`pie_name`,`main_ingredient`) VALUES ("whipped cream pie","sugar");
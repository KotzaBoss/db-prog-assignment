
CREATE TABLE IF NOT EXISTS `movies` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `mov_title` varchar(255) default NULL,
  `mov_year` mediumint default NULL,
  `mov_time` mediumint default NULL,
  `mov_lang` varchar(255),
  `move_country_release` varchar(100) default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Shawshank Redemption",1947,156,"English","Ireland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Good, The Bad and the Ugly",1998,178,"English","Bouvet Island");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Godfather",1976,66,"English","Guam");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Dark Knight",1986,168,"Hungarian","Hungary");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("12 Angry Men",2003,137,"English","Monaco");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Schindler's list,",1954,106,"English","Botswana");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Lord of the Rings",2003,152,"English","Bermuda");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Pulp Fiction",1947,63,"Maltese","Malta");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Fight Club",1962,136,"Farsi","Iraq");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Forrest Gump",1930,158,"Icelandic","Iceland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Inception",2019,49,"English","Poland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Star Wars",1943,98,"English","Kenya");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Matrix",1999,67,"English","Congo");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Goodfellas",1994,75,"English","Saint Barthélemy");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("One Flew over the Cuckoo's nest",1990,109,"English","Armenia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Seven Samurai",2011,81,"French","French Guiana");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("se7en",1990,167,"English","Maldives");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Life is Beautiful",1956,94,"Spanish","Ecuador");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("City of god",2016,173,"English","Martinique");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Silence of the Lambs",2014,69,"Spanish","Costa Rica");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("It's a Wonderful Life",2001,151,"English","Tuvalu");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Saving Private Ryan",1964,100,"English","Mali");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Green Mile",1988,169,"English","Austria");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Spirited Away",1939,52,"English","Australia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Interstellar",1973,134,"English","Dominican Republic");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Parasite",1994,82,"English","Ireland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Leon",1925,120,"English","Croatia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Hara-Kiri",1967,171,"English","Djibouti");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Usual Suspects",1943,56,"Hungarian","Hungary");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Lion King",2002,53,"English","South Sudan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Pianist",1935,144,"English","Nicaragua");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Back to the Future",1986,156,"English","Guam");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Terminator",1995,153,"English","Singapore");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Modern Times",1925,119,"English","Sint Maarten");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Gladiator",1956,133,"English","Georgia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Psycho",1978,129,"English","Rwanda");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("City of Lights",1990,113,"English","Indonesia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Departed",1956,92,"Swedish","Sweden");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Intouchables",1988,80,"English","Kenya");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Whiplash",1954,141,"English","Samoa");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Grave of the Fireflies",1990,103,"English","Mali");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Prestige",1951,118,"English","Viet Nam");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Once Upon a Time in the West",1965,93,"English","Cayman Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Casablanca",1959,127,"English","Ukraine");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Cinema Paradiso",1957,127,"English","UK");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Rear Window",1951,78,"English","Burkina Faso");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Alien",2016,61,"English","Brunei");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Apocalypse Now",1993,124,"English","Malawi");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Memento",1956,173,"English","Burundi");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Great Dictator",1926,124,"English","Libya");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Indiana Jones",1964,176,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Django Unchained",1989,81,"English","Montserrat");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Hamilton",2015,79,"English","Ecuador");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Lives of Others",1933,72,"Cantonese","China");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Paths of Glory",2003,66,"English","Samoa");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Joker",1964,104,"English","Niger");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("WALL-E",2018,88,"English","Niue");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Shining",1994,65,"English","Monaco");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Avengers",1985,154,"English","Barbados");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Sunset Boulevard",2021,142,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Witness for the Prosecution",2017,177,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Oldboy",1988,87,"English","Mauritius");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Spider-Man",1993,166,"English","Kiribati");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Princess Mononoke",1948,80,"Italian","Italy");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Dr. Strangelove",2009,132,"English","Syria");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Batman",1990,152,"English","Åland Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Once Upon a Time in America",1942,63,"English","Trinidad and Tobago");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Your Name.",1962,101,"English","Rwanda");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Capernaum",1935,111,"English","Kazakhstan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Braveheart",1955,171,"English","Central African Republic");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Das Boot",1977,142,"English","Marshall Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Toy Story",2014,143,"Korean","South Korea");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("3 Idiots",2007,151,"English","Niue");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("High and Low",2020,59,"French","Morocco");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Amadeus",1957,153,"English","Seychelles");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Inglourious Basterds",1981,162,"English","Tokelau");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Good Will Hunting",1999,167,"German","Germany");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Like Stars on Earth",2010,64,"Swedish","Norway");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Reservoir Dogs",1995,166,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("A Space Odyssey",1946,60,"English","Micronesia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Requiem for a Dream",1937,106,"English","Jamaica");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Vertigo",1946,138,"English","Afghanistan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("M",1931,145,"English","New Zealand");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("James Bond",1940,120,"English","Ecuador");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Hunt",2001,119,"English","Israel");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Eternal Sunshine of the Spotless Mind",1960,89,"English","Turkmenistan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Citizen Kane",1940,154,"English","Bahrain");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Dangal",1973,56,"English","Austria");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Singin' in the Rain",1989,153,"English","Slovakia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Bicycle Thieves",1936,179,"English","Liechtenstein");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Full Metal Jacket",1936,152,"English","Jordan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Kid",1988,116,"English","Suriname");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Come and See",1937,179,"English","Pakistan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Snatch",1960,94,"English","Niger");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("North by Northwest",2020,172,"German","Austria");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("A Clockwork Orange",1956,155,"English","Mauritania");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Scarface",1930,135,"English","Trinidad and Tobago");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Ikiru",1934,163,"English","Cape Verde");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("1917",1951,153,"English","Ghana");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Taxi Driver",1939,51,"English","Greece");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Icendies",2015,149,"Spanish","Costa Rica");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("A separation",1965,123,"English","Nepal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Lawrence of Arabia",1946,132,"English","Mauritius");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Amelie",1962,145,"English","Christmas Island");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Metropolis",1978,131,"English","Slovakia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("For a Few Dollars More",1999,180,"English","Cyprus");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Apartment",1938,139,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Double Idemnity",1985,110,"English","Argentina");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("To Kill a Mockingbird",2007,116,"English","Northern Mariana Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("UP",2010,78,"English","Northern Mariana Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Heat",1945,64,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("L.A. Confidential",2017,53,"English","American Samoa");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Die Hard",1953,116,"English","Iran");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Green Book",1946,69,"English","Myanmar");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Monty Python and the Holy Grail",1985,69,"English","Cayman Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Yojimbo",1973,132,"Japanese","Japan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Rashomon",2000,167,"English","Greece");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Downfall",1994,104,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Children of Heaven",1964,124,"English","Egypt");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Unforgiven",1925,130,"English","Palestine, State of");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Ran",1969,83,"English","Comoros");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Some Like It Hot",1975,112,"English","Suriname");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Howl's Moving Castle",1999,170,"English","Guyana");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("All About Eve",1987,83,"English","Serbia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Casino",1955,139,"English","Falkland Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("A beautiful Mind",1976,102,"English","Namibia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Wolf of Wall Street",1996,125,"English","Thailand");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Great Escape",1934,89,"English","Bonaire, Sint Eustatius and Saba");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Pan's Labyrinth",2001,158,"English","Reunion");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Secret in Their Eyes",1928,143,"Dutch","Netherlands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("There Will Be Blood",2011,170,"English","Rwanda");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Lock, Stock and Two Smoking Barrels",2005,165,"French","France");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Judgement at Nuremberg",2007,138,"English","Portugal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("My Neighbor Totoro",1955,90,"English","Solomon Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Raging Bull",1966,90,"English","Brunei");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Treasure of the Sierra Madre",1990,57,"English","Kyrgyzstan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Dial M for Murder",1967,163,"English","Brazil");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Three Billboards Outside Ebbing, Missouri",1960,55,"English","Denmark");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Shutter Island",1989,57,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Gold Rush",1981,63,"English","Samoa");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Chinatown",1954,105,"Icelandic","Denmark");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("My Father and My Son",1988,157,"English","Bonaire, Sint Eustatius and Saba");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("No Countryfor Old Men",2013,55,"English","Burkina Faso");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("V for Vendeta",1935,145,"Vietnamese","Viet Nam");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Inside Out",2017,116,"English","Antigua and Barbuda");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Elephant Man",1971,123,"English","Liberia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Thing",2010,84,"English","Tonga");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Seventh Seal",1981,97,"English","Burkina Faso");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Warrior",1956,153,"Dutch","Saint Martin");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Sixth Sense",1926,115,"English","Falkland Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Jurassic Park",1945,103,"Dutch","Mauritius");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Trainspotting",2019,143,"Moroccan","Morocco");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Klaus",1934,55,"Farsi","Iran");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Truman Show",1998,143,"English","Palau");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Gone with the Wind",2001,59,"English","Montserrat");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Finding Nemo",1969,119,"English","Philippines");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Wild Strawberries",1977,103,"English","Libya");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Stalker",1988,109,"English","Slovakia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Blade Runner",1994,172,"Dutch","Netherlands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Kill Bill",1936,141,"English","Macao");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Memories of Murder",2012,100,"English","Senegal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Bridge on the River Kwai",1975,177,"English","Nepal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Fargo",1990,106,"English","UK");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Room",1948,179,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Wild Tales",1969,130,"English","Sweden");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Gran Torino",1987,56,"English","Barbados");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Raatchasan",1984,63,"English","Lesotho");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Third Man",1976,86,"English","Jamaica");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Tokyo Story",2005,62,"English","Congo");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("On the Waterfront",1945,119,"French","France");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Deer Hunter",1984,103,"English","Saudi Arabia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("In the Name of the Father",1935,135,"English","Senegal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Mary and Max",1997,170,"English","Greece");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Grand Budapest Hotel",1970,74,"English","Viet Nam");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Before Sunrise",2000,174,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Gone Girl",2012,100,"English","Peru");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Catch Me If You Can",1928,166,"English","Saint Pierre and Miquelon");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Hacksaw Ridge",1996,159,"English","Turkey");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Prisoners",1983,100,"French","Switzerland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Persona",1999,55,"Spanish","Spain");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Andhadhun",1965,166,"English","Argentina");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Anand",1968,48,"English","Serbia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Sherlock Jr.",2020,134,"Danish","Faroe Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Sherlock Holmes",1982,98,"Spanish","Costa Rica");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Big Lebowski",1999,131,"English","United States");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Autumn Sonata",1933,51,"English","Moldova");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("To Be or Not to Be",2015,115,"English","Ghana");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The General",1927,174,"English","Kuwait");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Barry Lyndon",1974,159,"English","Kyrgyzstan");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("How to Train Your Dragon",1988,98,"German","Germany");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("For v Ferrari",2011,172,"English","Philippines");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Bandit",1952,177,"English","Zimbabwe");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("12 Years a Slave",1930,145,"Le Petit-Quevilly","The Netherlands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Mr. Smith Goes to Washington",1983,129,"Alma","Myanmar");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Mad Max",2016,147,"English","Bangladesh");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Dead Poets Society",1958,156,"English","Solomon Islands");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Million Dollar Baby",1954,46,"English","Ireland");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Network",1961,104,"English","Portugal");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("Stand By Me",1968,113,"English","Australia");
INSERT INTO `movies` (`mov_title`,`mov_year`,`mov_time`,`mov_lang`,`move_country_release`) VALUES ("The Handmaiden",1976,142,"English","Mongolia");
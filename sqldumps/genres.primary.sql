
CREATE TABLE IF NOT EXISTS `genres` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `gen_title` varchar(255) default NULL,
  PRIMARY KEY (`id`)
) AUTO_INCREMENT=1;

INSERT INTO `genres` (`gen_title`) VALUES ("Action");
INSERT INTO `genres` (`gen_title`) VALUES ("Adventure");
INSERT INTO `genres` (`gen_title`) VALUES ("Comedy");
INSERT INTO `genres` (`gen_title`) VALUES ("Crime");
INSERT INTO `genres` (`gen_title`) VALUES ("Fantasy");
INSERT INTO `genres` (`gen_title`) VALUES ("Historical");
INSERT INTO `genres` (`gen_title`) VALUES ("Horror");
INSERT INTO `genres` (`gen_title`) VALUES ("Romance");
INSERT INTO `genres` (`gen_title`) VALUES ("Science Fiction");
INSERT INTO `genres` (`gen_title`) VALUES ("Thriller");
INSERT INTO `genres` (`gen_title`) VALUES ("Western");
INSERT INTO `genres` (`gen_title`) VALUES ("Comedy");
INSERT INTO `genres` (`gen_title`) VALUES ("Drama");
INSERT INTO `genres` (`gen_title`) VALUES ("Cyberpunk");

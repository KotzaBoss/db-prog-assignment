CREATE TABLE `restaurants` (
  `id` mediumint(8) unsigned NOT NULL,
  `restaurant_name` varchar(255),
  `pie_id` int,
  `quantity_sold` mediumint,
  `city_located` varchar(255),
  PRIMARY KEY (`id`)
);

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (1,"Cheese cake factory",1,21,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (2,"Cheese cake factory",2,8,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (3,"Cheese cake factory",3,3,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (4,"Cheese cake factory",4,4,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (5,"Cheese cake factory",5,17,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (6,"Cheese cake factory",6,12,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (7,"Cheese cake factory",7,8,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (8,"Cheese cake factory",8,0,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (9,"Cheese cake factory",9,5,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (10,"Cheese cake factory",10,7,"New York");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (11,"Apple alley bakery",1,5,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (12,"Apple alley bakery",2,34,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (13,"Apple alley bakery",3,6,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (14,"Apple alley bakery",4,0,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (15,"Apple alley bakery",5,1,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (16,"Apple alley bakery",6,7,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (17,"Apple alley bakery",7,14,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (18,"Apple alley bakery",8,3,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (19,"Apple alley bakery",9,12,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (20,"Apple alley bakery",10,22,"New York");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (21,"Downtown bakery goods",1,5,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (22,"Downtown bakery goods",2,19,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (23,"Downtown bakery goods",3,1,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (24,"Downtown bakery goods",4,15,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (25,"Downtown bakery goods",5,24,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (26,"Downtown bakery goods",6,19,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (27,"Downtown bakery goods",7,10,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (28,"Downtown bakery goods",8,17,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (29,"Downtown bakery goods",9,2,"New York");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (30,"Downtown bakery goods",10,15,"New York");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (31,"LA pies",1,1,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (32,"LA pies",2,12,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (33,"LA pies",3,22,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (34,"LA pies",4,3,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (35,"LA pies",5,25,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (36,"LA pies",6,12,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (37,"LA pies",7,23,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (38,"LA pies",8,20,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (39,"LA pies",9,15,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (40,"LA pies",10,16,"Los Angeles");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (41,"East end bakeries",1,3,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (42,"East end bakeries",2,19,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (43,"East end bakeries",3,6,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (44,"East end bakeries",4,18,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (45,"East end bakeries",5,6,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (46,"East end bakeries",6,20,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (47,"East end bakeries",7,1,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (48,"East end bakeries",8,14,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (49,"East end bakeries",9,23,"Los Angeles");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (50,"East end bakeries",10,5,"Los Angeles");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (51,"Penns bakery",1,2,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (52,"Penns bakery",2,17,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (53,"Penns bakery",3,22,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (54,"Penns bakery",4,19,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (55,"Penns bakery",5,3,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (56,"Penns bakery",6,17,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (57,"Penns bakery",7,6,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (58,"Penns bakery",8,8,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (59,"Penns bakery",9,3,"Las Vegas");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (60,"Penns bakery",10,8,"Las Vegas");

INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (61,"Miami beach pies",1,10,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (62,"Miami beach pies",2,6,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (63,"Miami beach pies",3,10,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (64,"Miami beach pies",4,11,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (65,"Miami beach pies",5,12,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (66,"Miami beach pies",6,17,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (67,"Miami beach pies",7,7,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (68,"Miami beach pies",8,13,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (69,"Miami beach pies",9,16,"Miami");
INSERT INTO `restaurants` (`id`,`restaurant_name`,`pie_id`,`quantity_sold`,`city_located`) VALUES (70,"Miami beach pies",10,6,"Miami");
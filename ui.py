from utils import Pr
from queries import Q
from collections import namedtuple
from string import Formatter

Opt = namedtuple('Opt', ['descr', 'query', 'res_col_names'])
OPTS = {'x': Opt('Exit program', 'EXIT', ()),
        '1': Opt('Quantities sold and eaten per favourite pie',
                 Q.FAV_PIE_EATEN_SOLD,
                 ('Pie Name', 'Quantity Sold', 'Quantity Eaten')
                 ),
        '2': Opt('City that attracts most visitors',
                 Q.CITY_MOST_VISITORS,
                 ('City', 'Visitors')
                 ),
        '3': Opt('Favourite pie of at least 3 people in a city that is sold less than 20 there',
                 Q.PIE_FAV_3_SOLD_LT_8,
                 ('Pie Name','City Name','Num of pie favourites','Num of pies sold')
                 ),
        '4': Opt('Favourite ingredient of people',
                 Q.FAV_INGREDIENT,
                 ('Ingredient', 'Count')
                 ),
        '5': Opt('City that sells most pies of ingredient X',
                 Q.CITY_MOST_SOLD_OF_INGR_X,
                 ('City', 'Sold')
                 ),
        }

OPT_DISP = '\n'.join(f"{num}: {opt.descr}" for num, opt in OPTS.items())

def is_formatable(str):
    """ Check if there are any format fields in `str`"""
    return any(tup[1] for tup in Formatter().parse(str))

def ui_get_query(prompt=f'{OPT_DISP}\n>>> '):
    """ Display options and return corresponding query. """
    while (uin := input(prompt)) not in OPTS.keys():
        pass
    query = OPTS[uin].query
    colnames = OPTS[uin].res_col_names
    if uin == '5':
        query = query.format(ingr=input('Enter ingredient X: '))
    return uin, query, colnames

def ui_wait_to_return(prompt='Press ENTER to return'):
    input(prompt)
    Pr.bar()

import re
import sys

from mysql import connector
from os import listdir

from ui import ui_wait_to_return, ui_get_query
from hyperparams import USR, PSWD, SOCKET, DB_NAME, SQL_DUMP_DIR
from queries import Q, Conn
from utils import Pr, tabulate
from utils import ProgrammingError, InterfaceError

#
# Try to connect with default parameters else try with unix socket
#
Pr.section('Connecting to MySql')
try:
    CONN = Conn(connector.connect(user=USR,
                                  password=PSWD,
                                  host='localhost'
                                  ))
except InterfaceError:
    Pr.transition().subsection('Couldnt connect to database trying with unix_socket')
    try:
        CONN = Conn(connector.connect(user=USR,
                                      password=PSWD,
                                      unix_socket=SOCKET,
                                      host='localhost'
                                      ))
    except InterfaceError:
        Pr.fail().subsection('Couldnt connect with unix_socket').fail('EXITING')
        sys.exit(1)
Pr.done()



#
# DB connection
#
Pr.section(f'Connecting to {DB_NAME}')
try:
    CONN.execute_q(Q.USE_DB.format(name=DB_NAME))
except ProgrammingError as e:
    Pr.fail().subsection(f'Creating database {DB_NAME}')
    CONN.execute_q(Q.CREATE_DB.format(name=DB_NAME))
    CONN.execute_q(Q.USE_DB.format(name=DB_NAME))
Pr.done()

#
# Dumping tables
#
table_dump_files = (file for file in listdir(SQL_DUMP_DIR) if file.endswith(".table.sql"))

Pr.section(f'Dumping').transition()
for dump_file in table_dump_files:
    Pr.subsection(dump_file)
    if CONN.exists(dump_file.split('.')[0]):
        Pr.done('Exists')
        continue
    CONN.dump(f'{SQL_DUMP_DIR}/{dump_file}')
    Pr.done()

Pr.section('Creating view')
if CONN.exists(Q.VIEW_FAV_RESTAURANTS['name']):
    Pr.done('Exists')
else:
    CONN.execute_q(Q.VIEW_FAV_RESTAURANTS['query'])
    Pr.done()

#
# UI
#
Pr.section('Initialisation').done('Complete').bar()
while True:
    i, q, colnames = ui_get_query()
    if i == 'x':
        break
    res = CONN.execute_q(q).fetchall()
    print(tabulate(res, colnames))
    ui_wait_to_return()

Pr.section('Exiting').transition().subsection(f'Commiting changes to {DB_NAME}')
CONN.commit()
Pr.done()
Pr.subsection(f"Closing connection to {DB_NAME}")
CONN.close()
Pr.done()

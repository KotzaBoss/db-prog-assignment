from pathlib import Path

ROOT_DIR = Path(__file__).parent
SQL_DUMP_DIR = ROOT_DIR.joinpath('sqldumps')

USR = 'root'
PSWD = 'root'
SOCKET = '/run/mysqld/mysqld.sock'

DB_NAME = 'Husen_Kotzampopoulos'

MYSQL_NULL = 'null'
REPR_NULL = '<null>'

import re
from collections import namedtuple
from sys import stderr
from typing import Sequence, Union, Generator

from hyperparams import MYSQL_NULL
from utils import isnan

# Namedtuple of all queries needed in the program
Q = namedtuple('Queries', ['USE_DB', 'CREATE_DB', 'DROP_DB', 'DROP_TABLE', 'SHOW_TABLES_LIKE',
                           'VIEW_FAV_RESTAURANTS', 'FAV_PIE_EATEN_SOLD',
                           'CITY_MOST_VISITORS', 'PIE_FAV_3_SOLD_LT_8',
                           'FAV_INGREDIENT', 'CITY_MOST_SOLD_OF_INGR_X'])(
    USE_DB="USE {name}",
    CREATE_DB="CREATE DATABASE {name}",
    DROP_DB="DROP DATABASE {name}",
    DROP_TABLE="DROP TABLE {name}",
    SHOW_TABLES_LIKE="SHOW TABLES LIKE '{name}'",
    VIEW_FAV_RESTAURANTS={'name': 'fav_restaurants_of_people',
                          'query': """create or replace view fav_restaurants_of_people
                                      as
                                      select distinct restaurant_name, person_name, city_located, lives_in, favourite_pie
                                      from restaurants
                                      join people
                                      on people.favourite_restaurant = restaurants.restaurant_name
                                      order by restaurant_name
                                   """
                          },

    FAV_PIE_EATEN_SOLD="""select pie_name, qs, sum(quantity_eaten)
                          from people
                          join
                              (select pie_name, sum(quantity_sold) as qs
                                  from restaurants
                                  join pies
                                  on restaurants.pie_id = pies.id
                                  group by pie_name) as T
                              on people.favourite_pie = T.pie_name
                          group by pie_name
                       """,
    CITY_MOST_VISITORS="""select city_located, count(person_name) as visitors
                            from fav_restaurants_of_people
                            group by city_located
                            order by visitors desc
                            limit 1
                            """,
    PIE_FAV_3_SOLD_LT_8="""select pie_name, city_name, fav_count, sales
                            from
                                (select favourite_pie as pie_name, lives_in as city_name, fav_count, sales
                                from
                                    (select favourite_pie, lives_in, count(favourite_pie) as fav_count
                                    from
                                    people
                                    group by favourite_pie, lives_in) as T2
                                        join
                                            (select pie_name, city_located, sum(quantity_sold) as sales
                                            from
                                            pies
                                                join
                                                restaurants
                                            on pies.id = restaurants.pie_id
                                            group by city_located, pie_name) as T1
                                    on pie_name = favourite_pie and city_located = lives_in) as T3
                            where fav_count > 3 and sales < 20
                        """,
    FAV_INGREDIENT="""select main_ingredient, count(main_ingredient) as ing
                        from
                          (select pie_name, main_ingredient, person_name
                          from people
                          join pies
                          on people.favourite_pie = pies.pie_name) as T
                      group by main_ingredient
                      order by ing desc
                      limit 1
                   """,
    CITY_MOST_SOLD_OF_INGR_X="""select city_located as city_name, sum(quantity_sold) as qs
                                from
                                    pies
                                    join
                                    restaurants
                                    on restaurants.pie_id = pies.id
                                    where main_ingredient = '{ingr}'
                                group by city_name
                                order by qs desc
                                limit 1
                                           """,
)

class Conn:
    def __init__(self, conn):
        self.conn = conn
        self.cursor = conn.cursor()

    def execute_q(self, q):
        """ Call cursor.execute to execute the `query` and return cursor for further maipulation"""
        self.cursor.execute(q)
        return self.cursor

    def exists(self, table):
        """ Run a SHOW TABLES LIKE table query and check if `fetchall` return object considered True/False"""
        return bool(self.execute_q(Q.SHOW_TABLES_LIKE.format(name=table)).fetchall())

    def insert(self, table, rows: Union[Sequence[Sequence], Generator]):
        """ Insert all the rows in `rows` in table `table` and return `CURSOR`"""
        backtick_surround = lambda x: f'"{x}"'  # surround with " because ' may be used as Yoda's
        fmt_values = ''
        for row in rows:
            fmt_values += f'\t({", ".join(MYSQL_NULL if isnan(value) else backtick_surround(value) for value in row)}),\n'
        fmt_values = fmt_values[:-2]  # remove last ',\n' which should be ';' added in the query in Q.INSERT_
        query = Q.INSERT_PLANETS if table == PLANETS_T else Q.INSERT_SPECIES
        return self.execute_q(query(fmt_values))

    def dump(self, sql_file: str, debug=False):
        """ Open `sql_file` and execute instructions """
        with open(sql_file) as f:  # Hacky but no need to overkill
            content = f.read()
            if matches := re.finditer(r"(?P<cmd>\w[^;]+;)", content, re.MULTILINE):
                for match in matches:
                    if debug:
                        print(f"From {sql_file} exec: {match.group('cmd')}", file=stderr)
                    self.execute_q(match.group('cmd'))

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()
        self.cursor.close()

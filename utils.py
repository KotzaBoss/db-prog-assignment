import math
from operator import truth
from typing import Sequence, Final

from mysql import connector

from hyperparams import REPR_NULL

ProgrammingError = connector.ProgrammingError
InterfaceError = connector.InterfaceError

USAGE = None


def isnan(x) -> bool:
    """ Check if x is nan *after* making sure its a float.
        Pandas seems to return str when non uniform values per column are present except
        for NaN which is returned as float.
    """
    return isinstance(x, float) and math.isnan(x)


def sanatize_comma_sep_str(s) -> str:
    """ Make sure s has no extraneous spacesin between the values.
        a, b , c,d -> a,b,c,d
    """
    return ','.join(filter(truth, map(str.strip, s.split(','))))


def row_na_to_null(row: dict) -> dict:
    """ Return csv dict row with nan instead of 'NA' or 'N/A'"""
    return dict(map(lambda pair: (pair[0], math.nan) if pair[1] in ['NA', 'N/A'] else pair, row.items()))


def tabulate(data: Sequence, colnames: Sequence[str], padding: int = 1) -> str:
    """ Unecessary function that displays data in the MySQL cli table.
    Note to self: there is a python package called tabulate that does just that
    """
    pad = ' ' * padding
    max_col_width = max(map(len, (str(elem) for row in data for elem in row))) if data else 0
    max_header_width = max(map(len, colnames))
    final_max_width: Final = max(max_col_width, max_header_width)

    row_to_str = lambda entry: \
        f"|{'|'.join(f'{pad}{str(elem) if elem else REPR_NULL:<{final_max_width}}{pad}' for elem in entry)}|"  # str necessary for None

    plank = '+' + "+".join(f"{'':-<{final_max_width + 2 * padding}}" for _ in colnames) + '+'
    col_headers = row_to_str(colnames)

    body = '\n'.join(row_to_str(row) for row in data) + '\n' if data else ''
    end = plank + '\n' if data else ''

    return f"{plank}\n{col_headers}\n{plank}\n{body}{end}"


class Pr:
    """ Convinience namespace for debuggin prints.

        Basic usage: ::

            Pr.section('Testing even-ness').transition()
            for x in range(5):
                Pr.subsection(x)
                if not x % 2:
                    Pr.done('Even')
                else:
                    Pr.transition()
            Pr.done()

        Output: ::

            Testing even-ness...
                0... Even
                1...
                2... Even
                3...
                4... Even
            Done
    """
    DEFAULT_JUST_WIDTH = 30
    TAB = '\t'

    @classmethod
    def section(cls, msg, align: int = None):
        if not align:
            align = cls.DEFAULT_JUST_WIDTH
        print(f'{f"{msg}...":{align}}', end=' ')
        return Pr

    @classmethod
    def subnsection(cls, msg, n, align: int = None):
        if not align:
            align = cls.DEFAULT_JUST_WIDTH
        print(f'{n * cls.TAB}{f"{msg}...":{align}}', end=' ')
        return Pr

    @classmethod
    def subsection(cls, msg, align: int = None):
        return cls.subnsection(msg, 1, align)

    @classmethod
    def subsubsection(cls, msg, align: int = None):
        return cls.subnsection(msg, 2, align)

    @classmethod
    def transition(cls, prompt=''):
        print(prompt)
        return Pr

    @classmethod
    def done(cls, prompt='Done'):
        print(f'{prompt}')
        return Pr

    @classmethod
    def fail(cls, prompt='FAILED'):
        print(prompt)
        return Pr

    @classmethod
    def bar(cls, len=30, char='='):
        print(len * char)
        return Pr
